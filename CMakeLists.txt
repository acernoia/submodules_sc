cmake_minimum_required (VERSION 2.6)

include_directories ("include")

add_library (pc "src/pc.cpp")

add_executable (test_pc test/test_pc.cpp)

target_link_libraries (test_pc pc systemc)


enable_testing ()


add_test (Completes test_pc)


